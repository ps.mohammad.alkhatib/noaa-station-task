package com.progressoft.assignments.service;

import com.progressoft.assignments.data.DAO;
import com.progressoft.assignments.data.FileDAO;
import com.progressoft.assignments.data.FileInitializationException;
import com.progressoft.assignments.data.FileParser;
import com.progressoft.assignments.model.Station;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class Searcher {

    private final static Map<Integer, Function<Station, String>> mappers = new HashMap<Integer, Function<Station, String>>() {
        {
            this.put(1, Station::getUSAF);
            this.put(2, Station::getWBAN);
            this.put(3, Station::getStationName);
            this.put(4, Station::getCTRY);
            this.put(5, Station::getST);
            this.put(6, Station::getCALL);
            this.put(7, Station::getLAT);
            this.put(8, Station::getLON);
            this.put(9, Station::getELEV);
            this.put(10, Station::getBEGIN);
            this.put(11, Station::getEND);
        }
    };


    private ArrayList<Integer> columnsNumbers;

    private DAO dao;

    public Searcher(DAO dao) throws ParserConfigurationException, IOException, SAXException {
        this.dao = dao;
        ArrayList<Integer> columns = getColumnNumbers();
        verifyColumnsNum(columns);
        this.columnsNumbers = columns;
    }

    public Searcher(DAO dao, ArrayList<Integer> columnsNumbers) {
        this.dao = dao;
        verifyColumnsNum(columnsNumbers);
        this.columnsNumbers = columnsNumbers;
    }

    public static Searcher getInstance(DAO dao, ArrayList<Integer> columnsNumbers) {
        return new Searcher(dao, columnsNumbers);
    }

    private void verifyColumnsNum(ArrayList<Integer> columnsNumbers) {
        for (Integer columnsNumber : columnsNumbers) {
            if (columnsNumber < 1 || columnsNumber > 11)
                throw new IllegalArgumentException("invalid columns nums");
        }
    }

    private ArrayList<Integer> getColumnNumbers() throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory bf = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = bf.newDocumentBuilder();
        // TODO this path will fail in production
        String projectPath = System.getProperty("user.dir");
        Document document = builder.parse(projectPath + "/src/main/resources/numconfig.xml");
        NodeList list = document.getElementsByTagName("column");

        ArrayList<Integer> columnNum = new ArrayList<>();
        for (int i = 0; i < list.getLength(); i++) {
            columnNum.add(Integer.parseInt(list.item(i).getTextContent()));
        }
        return columnNum;
    }

    public ArrayList<String> searchByName(String name) throws ParserConfigurationException, IOException, SAXException {
        return records(dao.searchByName(name));

    }

    public ArrayList<String> searchByCountryCode(String code) {
        return records(dao.searchByCountryCode(code));
    }

    public ArrayList<String> searchByGeoLocation(String lat, String lon) {
        return records(dao.searchByGeoLocation(lat, lon));
    }

    public ArrayList<String> searchByRangeID(String start, String end) {
        return records(dao.searchByRangeID(start, end));
    }


    private ArrayList<String> records(ArrayList<Station> stationList) {
        ArrayList<String> records = new ArrayList<>();
        for (Station station : stationList) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < columnsNumbers.size(); i++) {
                sb.append(getField(station, columnsNumbers.get(i)));
                sb.append("||");
            }
            records.add(sb.toString());
        }
        return records;
    }

    private static String getField(Station station, int option) {
        Function<Station, String> function = mappers.getOrDefault(option, (o) -> {
            throw new IllegalArgumentException("invalid column number");
        });
        return function.apply(station);
    }
}
