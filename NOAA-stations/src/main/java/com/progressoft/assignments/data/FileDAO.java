package com.progressoft.assignments.data;

import com.progressoft.assignments.model.Station;

import java.io.IOException;
import java.util.ArrayList;

public class FileDAO implements DAO {
    public static final int EARTH_RADIUS = 6371;
    private ArrayList<Station> stations;
    private FileParser parser;

    public FileDAO(FileParser parser) throws FileInitializationException, IOException {
        this.parser = parser;
        this.stations = parser.getListOfStations();
    }

    public ArrayList<Station> searchByName(String name) {
        // TODO repeated code
        int reqLength = name.length();
        ArrayList<Station> matchedList = new ArrayList<>();
        for (Station station : stations) {
            String req = station.getStationName().substring(0, reqLength);
            if (name.equals(req)) {
                matchedList.add(station);
            }
        }
        return matchedList;
    }

    public ArrayList<Station> searchByCountryCode(String countryCode) {
        ArrayList<Station> records = new ArrayList<>();
        for (Station station : stations) {
            String req = station.getCTRY();
            if (req.equals(countryCode)) {
                records.add(station);
            }
        }
        return records;
    }

    public ArrayList<Station> searchByRangeID(String startId, String endId) {
        int start = Integer.parseInt(startId);
        int end = Integer.parseInt(endId);
        ArrayList<Station> records = new ArrayList<>();
        for (Station station : stations) {
            int stationId = Integer.parseInt(station.getUSAF());
            if (stationId >= start && stationId <= end) {
                records.add(station);
            }
        }
        return records;
    }


    public ArrayList<Station> searchByGeoLocation(String lat, String lon) {
        ArrayList<Station> records = new ArrayList<>();
        for (Station station : stations) {
            if (!station.getLAT().isBlank() && !station.getLON().isBlank()) {
                String lat2 = station.getLAT();
                String lon2 = station.getLON();
                if (calcDistance(lat, lon, lat2, lon2) < 50) {
                    records.add(station);
                }
            }
        }
        return records;
    }

    public double toRadiant(String str) {
        double pi = 3.141592653589793238;
        double degree = Double.parseDouble(str);
        return degree * pi / 180.0;
    }

    public double calcDistance(String lat1, String lon1, String lat2, String lon2) {
        double lat1R = toRadiant(lat1);
        double lon1R = toRadiant(lon1);
        double lat2R = toRadiant(lat2);
        double lon2R = toRadiant(lon2);
        double sins = Math.sin(lat1R) * Math.sin(lat2R);
        double coss = Math.cos(lat1R) * Math.cos(lat2R);
        double subs = Math.cos(lon1R - lon2R);
        return Math.acos(sins + coss * subs) * EARTH_RADIUS;
    }


}
