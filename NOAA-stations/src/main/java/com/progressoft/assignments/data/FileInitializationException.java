package com.progressoft.assignments.data;

public class FileInitializationException extends Exception {


    public FileInitializationException(String message){
        super(message);
    }

}
