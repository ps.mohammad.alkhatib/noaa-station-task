package com.progressoft.assignments.model;

import java.util.Objects;

public class Station {
    private final String USAF;
    private final String WBAN;
    private final String stationName;
    private final String CTRY;
    private final String ST;
    private final String CALL;
    private final String LAT;
    private final String LON;
    private final String ELEV;
    private final String BEGIN;
    private final String END;

    // TODO introduce a builder or parameter object to reduce the complexity of this constructor
    public Station(String USAF, String WBAN, String stationName, String CTRY,
                   String ST, String CALL, String LAT, String LON,
                   String ELEV, String BEGIN, String END) {
        this.USAF = USAF;
        this.WBAN = WBAN;
        this.stationName = stationName;
        this.CTRY = CTRY;
        this.ST = ST;
        this.CALL = CALL;
        this.LAT = LAT;
        this.LON = LON;
        this.ELEV = ELEV;
        this.BEGIN = BEGIN;
        this.END = END;
    }

    public String  getUSAF() {
        return USAF;
    }

    public String getWBAN() {
        return WBAN;
    }

    public String getStationName() {
        return stationName;
    }

    public String getCTRY() {
        return CTRY;
    }

    public String getST() {
        return ST;
    }

    public String getCALL() {
        return CALL;
    }

    public String getLAT() {
        return LAT;
    }

    public String getLON() {
        return LON;
    }

    public String getELEV() {
        return ELEV;
    }

    public String getBEGIN() {
        return BEGIN;
    }

    public String getEND() {
        return END;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Station station = (Station) o;
        return Objects.equals(USAF, station.USAF) && Objects.equals(WBAN, station.WBAN) && Objects.equals(stationName, station.stationName) && Objects.equals(CTRY, station.CTRY) && Objects.equals(ST, station.ST) && Objects.equals(CALL, station.CALL) && Objects.equals(LAT, station.LAT) && Objects.equals(LON, station.LON) && Objects.equals(ELEV, station.ELEV) && Objects.equals(BEGIN, station.BEGIN) && Objects.equals(END, station.END);
    }

    @Override
    public int hashCode() {
        return Objects.hash(USAF, WBAN, stationName, CTRY, ST, CALL, LAT, LON, ELEV, BEGIN, END);
    }

    @Override
    public String toString() {
        return "Station{" +
                "USAF='" + USAF + '\'' +
                ", WBAN='" + WBAN + '\'' +
                ", stationName='" + stationName + '\'' +
                ", CTRY='" + CTRY + '\'' +
                ", ST='" + ST + '\'' +
                ", CALL='" + CALL + '\'' +
                ", LAT='" + LAT + '\'' +
                ", LON='" + LON + '\'' +
                ", ELEV='" + ELEV + '\'' +
                ", BEGIN='" + BEGIN + '\'' +
                ", END='" + END + '\'' +
                '}';
    }
}
