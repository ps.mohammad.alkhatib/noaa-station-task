package com.progressoft.assignments;

import com.progressoft.assignments.control.Controller;
import com.progressoft.assignments.data.FileDAO;
import com.progressoft.assignments.data.FileInitializationException;
import com.progressoft.assignments.data.FileParser;
import com.progressoft.assignments.service.Searcher;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Properties;

public class Main {

    public static void main(String[] args) throws IOException, FileInitializationException, ParserConfigurationException, SAXException {
        Properties prop = new Properties();
        String projectPath = System.getProperty("user.dir");
        // TODO close stream
        InputStream input = new FileInputStream(projectPath+"/src/main/resources/config.properties");
        prop.load(input);
        String path = prop.getProperty("filePath");
        Searcher searcher = new Searcher(new FileDAO(new FileParser(path)));
        Controller application = new Controller(searcher);
        application.run();
    }

}