package com.progressoft.assignments.control;

import com.progressoft.assignments.service.Pair;
import com.progressoft.assignments.service.Searcher;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.function.Function;
import java.util.function.Supplier;

public class Controller {
    private static final int END_OPTION = 5;
    private final PrintStream out = System.out;
    private Scanner in = new Scanner(System.in);

    private Searcher searcher;


    public Controller(Searcher searchr) {
        this.searcher = searchr;
    }

    public void run() throws ParserConfigurationException, IOException, SAXException {
        while (true) {
            showMenu();
            int option = readOption();
            if (option == END_OPTION)
                break;
            performOption(option);
        }
    }

    public void performOption(int option) throws ParserConfigurationException, IOException, SAXException {
        switch (option) {
            case 1 -> nameSearch();
            case 2 -> codeSearch();
            case 3 -> rangeSearch();
            case 4 -> geoLocationSearch();
            default -> throw new IllegalArgumentException("invalid option number");
        }
    }

    private void geoLocationSearch() {
        Pair pair = readPairOFValues();
        ArrayList<String> results =
                searcher.searchByGeoLocation(pair.getFirst(), pair.getSecond());
        printer(results);
    }

    private void rangeSearch() {
        handleOption(this::readPairOFValues, this::searchByIdRange);
    }

    private ArrayList<String> searchByIdRange(Pair pair) {
        return searcher.searchByRangeID(pair.getFirst(), pair.getSecond());
    }

    private <T> void handleOption(Supplier<T> inputReader, Function<? super T, ArrayList<String>> search) {
        T input = inputReader.get();
        ArrayList<String> results = search.apply(input);
        printer(results);
    }

    private void codeSearch() {
        // TODO read, filtering, then printing out is duplicate behavior
        String code = readCode();
        ArrayList<String> results = searcher.searchByCountryCode(code);
        printer(results);
    }

    private void nameSearch() throws ParserConfigurationException, IOException, SAXException {
        String name = readName();
        ArrayList<String> results = searcher.searchByName(name);
        printer(results);
    }

    private Pair readPairOFValues() {
        out.print("Enter first value:");
        String first = in.nextLine();
        out.println();
        out.print("Enter second value:");
        String second = in.nextLine();
        return new Pair(first, second);
    }

    private String readCode() {
        out.print("Enter country code:");
        return in.nextLine();
    }

    private void printer(ArrayList<String> strings) {
        for (String string : strings) {
            out.println(string);
        }
    }

    private String readName() {
        out.print("Enter station name:");
        return in.nextLine();
    }

    private int readOption() {
        out.print("your option:");
        return in.nextInt();
    }

    private void showMenu() {
        out.println("Enter your selection: ");
        out.println("    1-Search for station by name");
        out.println("    2-Search stations by country code");
        out.println("    3-Search stations by stations ID range");
        out.println("    4-Search stations by Geographical location");
        out.println("    5-END");
    }

    public void verifyIds(Pair pair) {
        if (!isNumeric(pair.getFirst()) || !isNumeric(pair.getSecond()))
            throw new IllegalArgumentException("should be numeric values to be range");
    }

    private boolean isNumeric(String str) {
        return str.matches("-?\\d+(\\.\\d+)?");
    }
}
