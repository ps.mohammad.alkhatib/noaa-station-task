package com.progressoft.assignments;


import com.progressoft.assignments.data.FileInitializationException;
import com.progressoft.assignments.data.FileParser;
import com.progressoft.assignments.model.Station;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;

public class FileParserTest {

    @Test
    public void givenStationFiles_whenGetListOfStations_thenRightListReturned() throws IOException, FileInitializationException {
        FileParser fileParser =new FileParser();
        ArrayList<Station> stations = fileParser.getListOfStations();
        int expectedSize = 29977;
        Assertions.assertEquals(expectedSize,stations.size());
        // TODO you should assert the content
        // TODO introduce a smaller file for test

    }
    @Test
    public void givenFileToParse_whenGetListOfStations_thenExpectedReturned() throws IOException, FileInitializationException {
      //720904 00299 DELAND MUNICIPAL AIRPORT SIDN US   FL KDED  +29.067 -081.283 +0024.1 20151209 20170923

        Station expectedStation = new Station("720904","00299",
                "DELAND MUNICIPAL AIRPORT SIDN","US","FL",
                "KDED","+29.067","-081.283",
                "+0024.1","20151209","20170923");
        FileParser fileParser =new FileParser();
        ArrayList<Station> stations = fileParser.getListOfStations();
        Assertions.assertTrue(stations.contains(expectedStation));
    }
//    @Test
//    public void givenBadPathInConfig_whenGetListOfStations_ExceptionThrown() throws IOException, FileInitializationException {
//        FileParser fileParser =new FileParser();
//        FileInitializationException ex = Assertions.assertThrows(FileInitializationException.class,
//                fileParser::getListOfStations);
//        Assertions.assertEquals("failed to open file", ex.getMessage());
//    }


}
