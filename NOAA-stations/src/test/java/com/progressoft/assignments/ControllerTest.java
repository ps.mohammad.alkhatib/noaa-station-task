package com.progressoft.assignments;

import com.progressoft.assignments.control.Controller;
import com.progressoft.assignments.data.FileDAO;
import com.progressoft.assignments.data.FileInitializationException;
import com.progressoft.assignments.data.FileParser;
import com.progressoft.assignments.service.Searcher;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.RepeatedTest;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.Random;

public class ControllerTest {

    @RepeatedTest(36)// TODO useless test
    public void givenBadOptionNumber_whenRunTheProgram_thenExceptionThrown() throws IOException, FileInitializationException, ParserConfigurationException, SAXException {
        Searcher searcher = new Searcher(new FileDAO(new FileParser()));
        Controller app = new Controller(searcher);
        int [] arr = {0,-10,6,9,500,1000};
        Random random = new Random();
        int option = arr[random.nextInt(6)];
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                ()->app.performOption(option));
        Assertions.assertEquals(exception.getMessage(),"invalid option number");
        // TODO where is the test for valid option
    }

}
