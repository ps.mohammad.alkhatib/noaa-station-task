package com.progressoft.assignments;

import com.progressoft.assignments.data.FileDAO;
import com.progressoft.assignments.data.FileInitializationException;
import com.progressoft.assignments.data.FileParser;
import com.progressoft.assignments.model.Station;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

public class FileDAOTest {

    @Test
    public void givenNonExistingName_whenSearchByName_thenEmptyListReturned() throws IOException, FileInitializationException {
        FileDAO fileDAO =new FileDAO( new FileParser());
        ArrayList<Station> stations = fileDAO.searchByName("bla_bla");
        Assertions.assertEquals(0,stations.size());
    }

    @Test
    public void givenExistName_whenSearchByName_thenExpectedStationReturned() throws IOException, FileInitializationException {
        Station expectedStation = new Station("720904","00299",
                "DELAND MUNICIPAL AIRPORT SIDN","US","FL",
                "KDED","+29.067","-081.283",
                "+0024.1","20151209","20170923");
        String name ="DELAND MUNICIPAL AIRPORT SIDN";
        // TODO repeated setup
        FileDAO fileDAO =new FileDAO( new FileParser());
        ArrayList<Station> stations =
                fileDAO.searchByName(name);
        Assertions.assertTrue(stations.contains(expectedStation));
        for (Station station : stations) {
            Assertions.assertTrue(station.getStationName().equals(name));
        }

    }


    @Test
    public void givenNonExistingCountryCode_whenSearchByCode_thenEmptyListReturned() throws IOException, FileInitializationException {
        FileDAO fileDAO =new FileDAO( new FileParser());
        ArrayList<Station> stations = fileDAO.searchByCountryCode("bla_bla");
        Assertions.assertEquals(0,stations.size());
    }

    @Test
    public void givenExistCountryCode_whenSearchByCode_thenExpectedStationListReturned() throws IOException, FileInitializationException {
        Station expectedStation = new Station("720904","00299",
                "DELAND MUNICIPAL AIRPORT SIDN","US","FL",
                "KDED","+29.067","-081.283",
                "+0024.1","20151209","20170923");
        String code = "US";
        FileDAO fileDAO =new FileDAO( new FileParser());
        ArrayList<Station> stations =
                fileDAO.searchByCountryCode(code);
        Assertions.assertTrue(stations.contains(expectedStation));
        // TODO you should assert how many records returned
        for (Station station : stations) {
            Assertions.assertTrue(Objects.equals(station.getCTRY(), code));
        }

    }

    @Test
    public void givenValidRange_whenSearchByIdRange_thenExpectedStationListReturned() throws IOException, FileInitializationException {
        Station expectedStation = new Station("720904","00299",
                "DELAND MUNICIPAL AIRPORT SIDN","US","FL",
                "KDED","+29.067","-081.283",
                "+0024.1","20151209","20170923");
        FileDAO fileDAO =new FileDAO( new FileParser());
        ArrayList<Station> stations =
                fileDAO.searchByRangeID("700000","730000");
        Assertions.assertTrue(stations.contains(expectedStation));
        for (Station station : stations) {
            Assertions.assertTrue(Integer.parseInt(station.getUSAF())>=700000
                    && Integer.parseInt(station.getUSAF())<=730000);
        }
    }

}
