package com.progressoft.assignments;

import com.progressoft.assignments.data.FileDAO;
import com.progressoft.assignments.data.FileInitializationException;
import com.progressoft.assignments.data.FileParser;
import com.progressoft.assignments.service.Searcher;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class SearcherTest {

    @Test
    public void givenInvalidColumnsNums_whenConstructSearcher_thenExceptionThrown() throws IOException, FileInitializationException, ParserConfigurationException, SAXException {
        ArrayList<Integer> columns = new ArrayList<>(Arrays.asList(20));
        FileDAO fileDAO = new FileDAO(new FileParser());
        IllegalArgumentException ex = Assertions.assertThrows(
                IllegalArgumentException.class,()->new Searcher(fileDAO,columns)
        );
        Assertions.assertEquals(ex.getMessage(),"invalid columns nums");

    }

    @Test
    public void givenValidNameAndColumnNums_whenSearch_thenExpectedReturned() throws IOException, FileInitializationException, ParserConfigurationException, SAXException {
        ArrayList<Integer> columns = new ArrayList<>(Arrays.asList(1,2,3,11));
        Searcher searcher = new Searcher(new FileDAO(new FileParser()),columns);
        String expectedResult = "720904||00299||DELAND MUNICIPAL AIRPORT SIDN||20170923||";
        ArrayList<String> results = searcher.searchByName("DELAND MUNICIPAL AIRPORT SIDN");
        Assertions.assertTrue(results.contains(expectedResult));
    }

    @Test
    public void given_when_then() throws IOException, FileInitializationException {
        ArrayList<Integer>columns = new ArrayList<>(Arrays.asList(1,4));
        Searcher searcher = new Searcher(new FileDAO(new FileParser()),columns);
        ArrayList<String> results = searcher.searchByRangeID("720000","730000");
        String expected = "720059||US||";
        Assertions.assertTrue(results.contains(expected));
    }
}
